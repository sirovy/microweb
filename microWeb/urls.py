from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
	url('^markdown/', include( 'django_markdown.urls')),
    url(r'^admin/', include(admin.site.urls)),
#    url(r'^contact/', include('contact.urls')),
    url(r'^$', 'microWeb.views.getSite', name='name'),
    url(r'^(?P<name>[a-zA-Z\-]+)/$', 'microWeb.views.getSite', name='name'),
    #url(r'^(.*?)$', 'microWeb.views.getSite', name='notfound')
)


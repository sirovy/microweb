# views.py


from django.shortcuts import render_to_response
from microWeb.models import article, site, domain, theme, menu
import markdown
from django.contrib.sites.models import get_current_site
from django.http import HttpResponseNotFound
from microWeb.settings import MEDIA_URL
from datetime import datetime




def getSite(request, name=''):
    """
    nacteni dat z db a vykresleni do hml sablony, coz je v tuto chvili vlastne 
    veskera logika stranky, samotne spracovani dat se nechava na sablone, ke kterym
    je potreba napsat kvalitni dokumentaci a nejlepe nejaky genertor
    """ 
    try:
        hostname = str(get_current_site(request))
        SITE = domain.objects.filter(enabled='true').get(name=hostname).site    # pokud neprojde zachytava se domain.DoesNotExist:
        # natahneme konfiguraci z site, sablony a z settings.py
        data = {'hostname'      : hostname,
            'name'          : SITE.name,
            'media_url'     : MEDIA_URL,
            'css_path'      : SITE.theme.css_path,
            'logo_path'     : SITE.logo,
            'footer_text'   : SITE.footer_text}
        default_page = SITE.default_page.url
        # natahneme a seradime polozky pro hlavni menu stranky
        data['main_menu'] = SITE.main_menu.links.all().order_by('order')
        try:
            '''
            pokusime se najit clanek a pokud to nevyjde, vratime vychozi stranku dle default_page,
            kdyby nevyslo ani to, propadne vyjimka az na SERVER ERROR
            '''
            data['curent'] = name
            data['page'] = SITE.articles.filter(enabled='true').get(url=name)
        except article.DoesNotExist:
            """
            ARTICLE ERROR
            url neni nalezena, prechazim na vychozi stranku
            """
            data['curent'] = default_page
            data['page'] = SITE.articles.filter(enabled='true').get(url=default_page)
            # pokud dojde k zadani neexistujici url, da se spatna adresa do zavorky za titulek defaultni stranky
            if len(name) >= 1: data['page'].title += str(' ( neznama podstranka: ' + name + ' )') 
        data['page'].text = markdown.markdown(data['page'].text, ['tables'])
        try:
            data['page'].sub_menu = data['page'].submenu.links.all().order_by('order')
        except AttributeError:
            data['page'].sub_menu = None
        return render_to_response('base.html', data)
    except domain.DoesNotExist:
        '''
        DOMAIN ERROR
        pokud neni na serveru domena definovana, muze za to vetsinou zakaznik (nenastavil ji, nebo neni zaplaceno).

        TODO:  redirect na obednavaci formular se seznamem doporucovanych domen
        '''
        DOMAINS = str()
        #for h in domain.objects.filter(enabled='true').all():
        #return render_to_response('base.html', data)
        return HttpResponseNotFound('<h1>Hostname not found on this server.</h1>')
    except menu.DoesNotExist:
        '''
        SERVER ERROR
        V pripade jakekoli jine vyjimky se hodi hlaska o udrzbe serveru a posle se email adminovi, 
        aby to vypadalo vice planovane hlasi se cas zbyvajici do konce vypadku jako by do konce
        hodiny, pokud by zbyvalo mene nez 10 minut, nahlasi se dalsich 15 minut navic...
        '''
        NOW = datetime.now().minute + 2
        if NOW > 50: NOW = NOW - 15
        zbyva = 60 - NOW
        return HttpResponseNotFound('<h1>Sorry...</h1><BR><H3>service upgrade in progress, please try it again after %s minutes...</H3>' % str(zbyva))